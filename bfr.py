#!/usr/bin/env python
# pylint: disable=W0511
"""
This program obtains the filenames from a grep output file (part before ':')
and creates a bash script that utilizes sed to replace lines in files found
on the grep output file.
4/5/2016
Ricardo Castilla
"""

from __future__ import print_function
import argparse
import re
import os
import sys

SED_TEMPL = "sed -i -E -e s/{}/{}/ \"{}\""

def check_lower_bound(val_to_check):
    """
    This function is used by argparse in order to check the repeat_first integer
    gotten from the flag
    """
    val_to_check = int(val_to_check)
    if val_to_check < 1:
        raise argparse.ArgumentTypeError("Minimum bandwidth is 1")
    return val_to_check

def extract_regex(quote_type):
    """
    This function accepts a string {single, double} and returns a regex
    that finds quoted text. The type of quote to look for will be decided by
    the given quote_type string
    """
    base_regex = r"{}([^{}]*){}"
    # decide what quote regex to use
    if quote_type == "single":
        return base_regex.format("\'", "\'", "\'")
    elif quote_type == "double":
        return base_regex.format("\"", "\"", "\"")
    elif quote_type == "parens":
        # regular expression to catch everything between parenthesis
        return base_regex.format("(", ")", ")")
    return None

def repeat_first(i_list, repeats):
    """
    This function accepts a list and an integers that specifies how many
    times the first element of the list needs to be repeated. If repeats
    is greater than the length of i_list, then the list is expanded. If it is
    less, the list is shrank. All elements in the list will be replaced with the
    first element.
    """
    for index in range(0, repeats):
        # TODO: Need to double check this for the case where repeats > 1
        if len(i_list) < repeats:
            i_list.append(re.escape(i_list[0]))
        else:
            i_list[index] = re.escape(i_list[0])
            # if the wanted repetitions are less than the list,
            # then shrink the list
            if len(i_list) > repeats:
                del i_list[repeats:]

def create_script(file_names, find, replace, excludes, repl_line=False,# pylint: disable=R0913,R0914
                  extract=None, repeat=False, rep_first=1):
    """
    Accepts multiple file_names (an array), and will output
    a file with each filename found. One output file per input given.
    If a filename includes a partial string defined in 'excludes', it will
    be ignored.
    if repl_line=True then the line containing the substring will be replaced.
    if extract=True then strings will be parsed to keep the quoted values.
    """
    ignore = False
    old_sed_regex = SED_TEMPL
    qt_regex = extract_regex(extract)
    # These parenthesis around find creates a capture group
    find = "({})".format(find)
    print("Find is: {}".format(find))
    if repl_line:
        # add .* to the regex to replace the whole line containing FIND
        # also, keep the last {} in order to add the split_line["filename"] later on
        # need to replace the first {} with the expanded regex per file to avoid
        # ambiguity
        old_sed_regex = old_sed_regex.format("{}" + ".*", "{}", "{}")

    for file_n in file_names:
        entered_names = {} # dictionary that will hold filenames. Faster than lists
        print("Parsing {}...".format(file_n))
        # create output name according to given file
        # open both files
        with open(file_n) as in_file:
            with open(os.path.expanduser(os.path.basename(file_n)) + ".bfr", "w") as out_file:
                # iterate through each line
                for lnum, line in enumerate(in_file):
                    split_line = {}
                    sed_regex = old_sed_regex
                    try:
                        split_line["filename"], split_line["string"] = line.split(":", 1)
                        # here we shrink the line to start at FIND. This allows
                        # correct quote/parens matching when using user's regex
                        # more hacky stuff
                        found_re = re.search(find, split_line["string"]) # find is now a regex obj
                        # convert find into the found string
                        found_re = found_re.group(1) # we use 1 to select our capture group
                        print("found_re:{}".format(found_re))
                        # enter the expanded regex found in the line into the sed regex
                        # adds file and line specific changes to sed. Erradicate
                        # ambiguity
                        sed_regex = sed_regex.format(re.escape(found_re),
                                                     re.escape(replace),
                                                     "{}")
                        split_line["string"] = \
                            split_line["string"][split_line["string"].index(found_re):]
                    except ValueError as exce:
                        print("Unable to parse line {}: {}: Error={}".format(lnum+1, line, exce))
                        continue
                    except AttributeError:
                        print("Did not find {} in line {}".format(find, lnum+1))
                        continue
                    # make sure we dont have repeated entries, unless asked to
                    if repeat or entered_names.get(split_line["filename"]) is None:
                        # if asked for, save the string between quotes
                        if extract is not None:
                            # find all instances of quoted text and store it
                            split_line["quoted"] = re.findall(qt_regex, found_re)
                            # if asked to repeat first quoted, then replace all
                            # with the first. Expand array if needed. Has to be done
                            # in order for .format to work correctly. Extra hacky
                            # TODO: Do not run this if ["quoted"] is empty. It may be due to
                            # wrong extract value given (single, double, parens, etc.)
                            if rep_first > 1:
                                repeat_first(split_line["quoted"], rep_first)
                            # This is done in order to be python 2.7 compatible
                            # because syntax does not allow extended unpacking until Py3k
                            # hacky, me knows
                            split_line["quoted"].append(split_line["filename"])
                            print("split_line[quoted]:{}".format(split_line['quoted']))
                            # re.escape adds '\' infront of {}. We need to remove it
                            # in order for .format to work
                            sed_regex = sed_regex.replace(r"\{\}", r"{}")
                            sed_regex = sed_regex.format(*split_line["quoted"])
                        else:
                            sed_regex = sed_regex.format(split_line["filename"])
                        # iterate through ignore list
                        for exclude in excludes:
                            if exclude.lower() in split_line["filename"].lower():
                                ignore = True
                        if not ignore:
                            # if file has not been parsed yet, make sure to back it up
                            if entered_names.get(split_line["filename"]) is None \
                                and not entered_names.get(split_line["filename"]):
                                out_file.write("fbak \"{}\" && ".format(split_line["filename"]))
                                entered_names[split_line["filename"]] = True
                            # add quotes around filename to fix special char problems (Spaces etc)
                            out_file.write("{};\n".format(sed_regex))
                    ignore = False
        print("Done")

if __name__ == "__main__":
    parser = argparse.ArgumentParser( # pylint: disable=C0103
        description='Build Find and Replace.\
        Creates a bash script that utilizes \
        fbak and sed to find and replace strings in all files given.\nInput files are \
        expected to be the output of running grep with the given \'find\' argument.')
    parser.add_argument('find', metavar="FIND", help='String to find')
    parser.add_argument('replace', metavar="REPLACE",
                        help='String to replace find with. May contain {} which is helpful \
                            in string interpolation using -r or -f.')
    parser.add_argument('files', metavar="<file>", nargs='+',
                        help='File to be parsed. Expected to be a grep output file.')
    parser.add_argument('--excludes', nargs=1, help='Filename or path substrings to exclude.\n\
                                Expected to be a comma separated list')
    parser.add_argument('-c', '--change-line', dest="change_line", action="store_true",
                        help="Alters behavior to replace \
                            entire line containing FIND. Line is replaced with the \
                            contents of REPLACE. Useful in conjunction with the -x flag")
    parser.add_argument('-x', '--extract', dest="extract",
                        type=str, choices=['single', 'double', 'parens'],
                        help="Extract substring between quotes and use it wherever {} \
                            is found withing REPLACE. Able to check single or doulbe quotes.\
                            Parens is useful when it is necessary to replace a function call, or\
                            other texts similar to if statements. As a result, values within\
                            the first parenthesis pair, including the parenthesis, will be \
                            kept. Include --remove-parens to only keep values within \
                            the parenthesis.")
    parser.add_argument('-r', '--repeat-files', action="store_true", dest="repeat",
                        default=False,
                        help="Check files to change more than once. Default is False")
    # TODO: Add flag to only read a specific number of quotes and use them in order
    # similar to -f but does not repeat the first element of the list.
    # Remember to escape strings in case they have space (re.escape)
    parser.add_argument('-f', '--repeat-first', dest="repeat_first", default=1, type=int,
                        help="Specify the amount of times to repeat first quoted string. \
                            Only the first quoted string will be used. This number must \
                            match with the amount of {} in the replace string.")
    parser.add_argument('-V', dest="verbose", action="store_true", # TODO: Complete this
                        default=False,
                        help="Allows the program to be verbose. It will output \
                            all its actions into a seperate file in readable english \
                            with the actions it will perform")
    args = parser.parse_args() # pylint: disable=C0103

    # create excludes
    if args.excludes is None:
        # if empty, this is the default list to ignore
        args.excludes = ["BAK", "Binary file", ".ORIG", "Backups", "sysalex", "test"]
    else:
        # split the excludes into a list if not empty
        args.excludes = args.excludes[0].split(',')

    # # escape find and replace strings if asked to
    # if args.escape_find:
    #     args.find = re.escape(args.find)
    # if args.escape_replace:
    #     args.replace = re.escape(args.replace)

    # if args.extract and args.repeat_first:
    create_script(args.files, args.find, args.replace, args.excludes,
                  args.change_line, args.extract, args.repeat,
                  args.repeat_first)
    # else:
    #     create_script(args.files, args.find, args.replace, args.excludes,
    #                   args.change_line, repeat=args.repeat)
