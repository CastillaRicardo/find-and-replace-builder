SOURCE=bfr.py
NEXEC=bfr.exe
EXEC=bfr

$(NEXEC): $(SOURCE)
	nuitka --python-version=2.7 --plugin-enable=pylint-warnings --recurse-all $(SOURCE)
	mv $(NEXEC) $(EXEC)

portable: $(SOURCE)
	nuitka --python-version=2.7 --plugin-enable=pylint-warnings --recurse-all --portable $(SOURCE)
	mv bfr.dist/$(NEXEC) bfr.dist/$(EXEC)

default: $(NEXEC)

clean:
	rm -rf *.build *.dist $(EXEC)
